# Introduction

This project creates a docker image with [react](https://react.dev/) installed.

The image can be used to build software using react.

The original repository is at https://git.sw4j.net/sw4j-net/react

This repository is mirrored to https://gitlab.com/sw4j-net/react
