ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/nodejs20:latest

RUN <<EOF
useradd -ms /bin/bash react
apt-get update
apt-get -y upgrade
EOF

WORKDIR /home/react
USER react
